# rbias

This package offers the possibility to analyse, visualise, and interpret the impact of modern built-up and infrastructural development on the archaeological site distribution.

Please check the files in the 'example' folder for instructions on how to use the package.

## Installation

You can install rbias from gitlab with:

```r
if(!require('devtools')) install.packages('devtools')
devtools::install_git("https://gitlab.com/CRC1266-A2/rbias")
```
